#include <iostream>
#include <vector>
#include <sstream>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <cstring>

#define TRUE 1

using namespace std;

void pwd()
{
  char dirPath[1024];
  if (getcwd(dirPath, sizeof(dirPath)) != NULL) {
    cout << dirPath << endl;
  }
  else {
    perror("getcwd() error");
    exit(0);
  }
}

void cat(const char *filePathChar)
{
  int fileDescripter, size;
  char *buf = (char *) calloc(100, sizeof(char));

  fileDescripter = open(filePathChar, O_RDONLY);
  if (fileDescripter < 0) {
    perror("open error");
    exit(1);
  }

  size = read(fileDescripter, buf, 100);
  buf[size] = '\0';
  cout << buf << endl;
}

void cd(const char *filePathChar)
{
  // converting string to char array
  string temp = "/Users";
  char *h = new char [temp.length()+1];
  strcpy (h, temp.c_str());


  if(filePathChar == NULL)
    chdir(h); // change directory
  else if ((strcmp(filePathChar, "~")==0) || (strcmp(filePathChar, "~/")==0))
    chdir(h); // going to root folder
  else if(chdir(filePathChar)<0) // no directory exit handling
    cout << "Error occur, there are no such directory named: " << filePathChar << " exit" << endl;
}

void execute(const vector<string>& tokens) {
  // Get command. Arguments are in tokens[1...]
  string cmd = tokens[0];
  string fileName = tokens[1];
  string folderName = tokens[1];
  const char *filePathChar = fileName.c_str();

  // TODO: handle shell commands
  if (cmd == "cd") {
    cd(filePathChar);
  } else if (cmd == "pwd") {
    pwd();
  } else if (cmd == "cat") {
    cat(filePathChar);
  } else if (cmd == "exit") {
	exit(0);
  } else {
    cout << "Unknown command" << endl;
  }
}

int main() {
  // Shell loop. Press ctrl+c to exit shell.
  while (TRUE) {
    cout << "> ";

    // Get input
    string line;
    getline(cin, line);
    stringstream ss(line);
    vector<string> tokens;
    string token;
    while (ss >> token) {
      tokens.push_back(token);
    }

    // Handle the command
    execute(tokens);
  }
}
